import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class RABPanel extends JPanel{
  PICSimMain parent;

  void saveParent(PICSimMain parent)
  {
    this.parent=parent;
  }
  
  int numy=20; // y loc for row of numbers
  int ray=40; // y loc for RA LEDs
  int rby=80; // y loc for RB LEDs
  int startx=50; // x loc for starting LEDs
  int diam=20; // diameter of LEDs
  int sep=25; // separation of LEDs
  
  int pwmX=275;
  int pwmY=60;
  int pwmDiam=50;
  
  public void paint(Graphics g)
  {
    super.paint(g);
    g.setColor(Color.BLACK);

    if (parent==null) return;
    if (parent.pins == null) return; // pin data not available yet

    // paint numbers
    for (int i=0;i<8;i++){
      g.drawString(""+(7-i), 10+startx+i*sep,numy);
    }
    
    // paint labels
    g.drawString("RA:",20,ray+diam/2);
    g.drawString("RB:",20,rby+diam/2);
    
    for (int row=0;row<2;row++){
      int rowIndex=(row==0)?Pin.RA:Pin.RB;
      
      for (int i=0;i<8;i++){
        boolean isOutput=(parent.pins[rowIndex][7-i].type == Pin.DOUT);
        int val=parent.pins[rowIndex][7-i].value;
        
        // fill in (in red) if a valid output and if == 1
        if ((val==1) && isOutput){ // LED is on (and is an output)
          g.setColor(Color.RED);
          g.fillOval(startx+i*sep,(row==0)?ray:rby-diam/2,diam,diam);
          g.setColor(Color.BLACK);
        }
        
        // always draw outline
        g.drawOval(startx+i*sep,(row==0)?ray:rby-diam/2,diam,diam); // outline
        
        // x-out if not an output
        if (!isOutput){ // not an output
          g.drawLine(startx+i*sep,
                     (row==0)?ray:rby-diam/2,
                     startx+i*sep + diam,
                     diam+((row==0)?ray:rby-diam/2));
          g.drawLine(startx+i*sep,
                     diam+((row==0)?ray:rby-diam/2),
                     startx+i*sep + diam,
                     (row==0)?ray:rby-diam/2);
        } // x'd out
      } // all LEDs done
    } // both rows
    
// draw PWM output (if checkbox is enabled)
    if (parent.pwmEnable.isSelected()){
      g.drawOval(pwmX,pwmY,pwmDiam,pwmDiam);
      double dx,dy;
      dx=(pwmDiam/2.)*Math.cos(parent.theta);
      dy=(pwmDiam/2.)*Math.sin(parent.theta);
      int xc=pwmX+pwmDiam/2;
      int yc=pwmY+pwmDiam/2; // center
      g.drawLine(xc,yc,(int)(xc+dx),(int)(yc+dy));
    }
  }
}
