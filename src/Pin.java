public class Pin {
  int type; // see statics below
  static int DIN=1;
  static int DOUT=2;
  static int AIN=3;

// first index in Pin[2][8] array
  static int RA=0;
  static int RB=1;

  int value; // 1 or 0 (digital) or 0-5000 (analog)
  
  // turn 'A' or 'B' into proper index
  static int INDEX(char c)
  {
    return ((c=='A')?Pin.RA:Pin.RB);
  }
}