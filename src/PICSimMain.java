import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.border.BevelBorder;
import javax.swing.JScrollBar;
import java.awt.event.AdjustmentListener;
import java.awt.event.AdjustmentEvent;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JSlider;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class PICSimMain extends JFrame {

	MainThread mainThread; // run the mdb command, monitor output
	File currentDir=null; // save last directory
	String[] mdbCommand={""}; // save this too

	private JPanel contentPane;
	PICSimMain parent=this;
	RABPanel rabPanel; // display output LEDs
	
	JCheckBox[] ra=new JCheckBox[8];
	JCheckBox[] rb=new JCheckBox[8];
	RABListener rabListener=new RABListener(this); // handles button clicks
	
	JSlider[] anSlider=new JSlider[7];
	JLabel[] anLabel=new JLabel[7]; // analog input
	ANListener anListener=new ANListener(this);
	
	JLabel pcLabel;
	Pin[][] pins=new Pin[2][8]; // [RA/RB][pin#]

	JCheckBox pwmEnable;

    double theta=-Math.PI/2; // increment whenever RB3=1
    double thetaDelta=.01; // step size (per cycle - random scale!)
    
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PICSimMain frame = new PICSimMain();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PICSimMain() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
			  System.out.println("Goodbye!");
			  mainThread.doExit();
			  //System.exit(0);
			}
		});
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 669, 575);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// read mdbCommand path and last-used dir
		readSavedData();
		
// read a .HEX file into mem
		JButton btnLoadFile = new JButton("Load File");
		btnLoadFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			  JFileChooser jfc;
			  File file;
			  
			  // open in latest directory
			  if (currentDir != null){
			    jfc=new JFileChooser(currentDir);
			  } else {
			    jfc=new JFileChooser();
			  }
			  FileNameExtensionFilter filter=new FileNameExtensionFilter("Hex files","hex");
			  jfc.setFileFilter(filter);
			  int returnValue=jfc.showOpenDialog(parent);
			  if (returnValue == JFileChooser.APPROVE_OPTION){ // user requested a file
			    file=jfc.getSelectedFile();
			    currentDir=file;
	            writeCurrentSavedDir(currentDir); // this will also save the mbdCommand
			    
			    //System.out.println("file=" + file);
			    // load this into simulator memory
			    mainThread.loadFile(file.getAbsolutePath());
			  }
			}
		});
		btnLoadFile.setBounds(29, 12, 117, 25);
		contentPane.add(btnLoadFile);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			  mainThread.doExit();
			}
		});
		btnExit.setBounds(503, 12, 117, 25);
		contentPane.add(btnExit);
		
		pcLabel = new JLabel("PC: ");
		pcLabel.setBounds(197, 17, 70, 15);
		contentPane.add(pcLabel);
		
		rabPanel = new RABPanel();
		rabPanel.saveParent(this); // so panel can get pin data
		rabPanel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		rabPanel.setBounds(167, 81, 360, 126);
		contentPane.add(rabPanel);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.addAdjustmentListener(new AdjustmentListener() {
			public void adjustmentValueChanged(AdjustmentEvent arg0) {
			  int delay=arg0.getValue(); // set inter-step delay
			  if (mainThread != null) mainThread.setDelay(delay);
			}
		});
		scrollBar.setMaximum(1000);
		scrollBar.setOrientation(JScrollBar.HORIZONTAL);
		scrollBar.setBounds(63, 522, 333, 15);
		contentPane.add(scrollBar);
		
		JLabel lblSpeed = new JLabel("SPEED:");
		lblSpeed.setBounds(0, 522, 70, 15);
		contentPane.add(lblSpeed);
		
		JLabel lblSlow = new JLabel("fast");
		lblSlow.setBounds(74, 501, 70, 15);
		contentPane.add(lblSlow);
		
		JLabel lblFast = new JLabel("slow");
		lblFast.setBounds(343, 501, 70, 15);
		contentPane.add(lblFast);
		
		pwmEnable = new JCheckBox("PWM");
		pwmEnable.setBounds(535, 139, 118, 23);
		contentPane.add(pwmEnable);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			  mainThread.doReset(); // request reset command
			}
		});
		btnReset.setBounds(503, 44, 117, 25);
		contentPane.add(btnReset);
		
		// analog control setup
		int yloc=243;
		for (int i=0;i<7;i++){
	      anSlider[i]=new JSlider(0,5000,0); // 0-5000, initial value=0
		  anSlider[i].setBounds(167, yloc, 289, 15);
		  anSlider[i].addChangeListener(anListener);
		  contentPane.add(anSlider[i]);
		  anLabel[i] = new JLabel("AN" + i + ": 0.000 V");
		  anLabel[i].setBounds(474, yloc, 117, 15);
		  yloc+=25;
		  contentPane.add(anLabel[i]);
		}
		
		for (int i=0;i<8;i++){
		  ra[i]=new JCheckBox("RA"+i);
		  ra[i].addActionListener(rabListener);
		  ra[i].setBounds(17,100+25*i,60,23);
		  contentPane.add(ra[i]);

		  rb[i]=new JCheckBox("RB"+i);
		  rb[i].addActionListener(rabListener);
		  rb[i].setBounds(80,100+25*i,60,23);
		  contentPane.add(rb[i]);
		}
		
// start the main thread
		mainThread=new MainThread(parent);
		mainThread.start();
		
		// initialize RAB flags
		initRAB();
	}

	void readSavedData()
	{
		try {
			Scanner sc=new Scanner(new File(System.getProperty("user.home") + File.separator + ".picsimrc"));
			mdbCommand[0]=sc.nextLine(); // path to mdb
			currentDir=new File(sc.nextLine()); //last-used directory for loading .hex file
			sc.close();
			return;
		} catch (Exception e1) {}
// need to create this
        try{
          PrintWriter pw=new PrintWriter(new File(System.getProperty("user.home") + File.separator + ".picsimrc"));
          // let user specify path to mdb
          JFileChooser jfc=new JFileChooser();
          jfc.setDialogTitle("Please locate the mdb tool");
          boolean doMore=true;
        
          while (doMore){
            JOptionPane.showMessageDialog(this, "You need to specify the location of the MicroChip debugger (mdb)\n" +
              "On linux, check around /opt/microchip/mplabx/v(#.##)/mplab_platform/bin/mdb.sh or /usr/bin/mdb\n" +
              "On Windows, try C:\\Program Files (x86)\\Microchip\\MPLABX\\v(#.##)\\mplab_ide\\bin\\mdb.bat");
            int returnValue=jfc.showOpenDialog(parent);
		    if (returnValue == JFileChooser.APPROVE_OPTION){ // user requested a file
		    	doMore=false;
		    } else System.exit(0); // repeating loop is annoying!
          }
          // user has specified the path to mdb...save this

		  mdbCommand[0]=jfc.getSelectedFile().getAbsolutePath();
		  pw.println(mdbCommand[0]); // save this for future runs
		  pw.println(System.getProperty("user.home")); // just a placeholder for now
		  pw.close();
        } catch (Exception e){
          JOptionPane.showMessageDialog(this, "Trying to save .picsimrc data in " +
            System.getProperty("user.home") + File.separator + ".picsimrc" +
            " but getting an error: " + e + 
            "\nSorry! Make sure you have write permission to your home dir and try removing .picsimrc and running this again.");
          System.exit(0);
		}
	}
	
	void writeCurrentSavedDir(File currentDir)
	{
	  try {
		PrintWriter pw=new PrintWriter(System.getProperty("user.home") + File.separator + ".picsimrc");
		pw.println(mdbCommand[0]); // save this again
		pw.println(currentDir.getAbsolutePath());
		pw.close();
	  } catch (Exception e) {
		System.out.println("Can't save default dir: " + e);
	  }
	}
	
	// handlers for InputThread's parser
	void setPC(String pc)
	{
	  pcLabel.setText("PC: " + pc);
	}
	
	int period=5000;
	int on=0; // update these continuously
	boolean lastValue=false;
	
	// called whenever an output pin value has changed
    void showRAB(char ab,int pin,boolean value)
    {
	  if (ab=='A'){
	    pins[Pin.RA][pin].value=value?1:0;
	  }

	  if (ab=='B'){
	    pins[Pin.RB][pin].value=value?1:0;
	  }
	  
	  // PWM output?
	  if ((ab=='B') && (pin==3)){ // we have a new reading on RB3 (P1A)
	    ++period; // increment this each sample
	    if (value){ // output is high
	      ++on; // how many ticks are we on?
	    }
	    if ((lastValue && (!value)) || (period > 2000)){ // just dropped low - end of cycle
	      double dutyCycle=((double)on)/((double)period);
	      //System.out.println("DC: " + dutyCycle);
	      // adjust thetaDelta
	      thetaDelta=.01*dutyCycle;
	      period=0;
	      on=0;
	    }
	    lastValue=value; // save for next time
	    
	    theta+=thetaDelta;
	    if (theta > 2*Math.PI) theta=theta-2*Math.PI;
	  }

	  showRAB(); // display new values
    }
    
// display LED value
    void showRAB()
    {
      rabPanel.repaint();
    }
    
    void initRAB()
    {
      for (int i=0;i<8;i++){
        pins[Pin.RA][i]=new Pin();
        pins[Pin.RA][i].type=Pin.DIN;
        pins[Pin.RB][i]=new Pin();
        pins[Pin.RB][i].type=Pin.DIN; // just to have something...
      }
      showRAB();
      
    }
    
    void raEnable(char ab,int pin,boolean isInput)
    {
      if (isInput) pins[Pin.INDEX(ab)][pin].type=Pin.DIN;
      if (ab=='A') ra[pin].setEnabled(isInput);
      if (ab=='B') rb[pin].setEnabled(isInput);
    }

    String[] anMap={"RA0","RA1","RA2","RA3","RB0","RB1","RB4"}; // AN0-AN6
    void anEnable(char ab,int pin,boolean isInput)
    {
      if (isInput){ // this is an analog input pin - enable the slider
        pins[Pin.INDEX(ab)][pin].type=Pin.AIN;
      }
      // now enable/disable the slider
      int i;
      for (i=0;i<anMap.length;i++){
        if (anMap[i].equals("R"+ab+pin)){ // found it!
          anSlider[i].setEnabled(isInput);
          anLabel[i].setVisible(isInput);
          return;
        }
      } // didn't find pin???
    }

    void ledEnable(char ab,int pin,boolean isOutput)
    {
      if (isOutput) pins[Pin.INDEX(ab)][pin].type=Pin.DOUT;
    }
}