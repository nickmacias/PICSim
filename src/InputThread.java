import java.util.Scanner;

public class InputThread extends Thread
{
  PICSimMain parent; // main frame

  Scanner sc; // used for reading stdout from mdb process
  InputThread(Process proc,PICSimMain parent)
  {
    this.parent=parent;
    sc=new Scanner(proc.getInputStream());
    //sce=new Scanner(proc.getErrorStream());
  }
  
  public void run()
  {
    while (sc.hasNextLine()){
      String buffer=sc.nextLine();
      parse(buffer); // process this
      //System.out.println("<" + buffer + ">");
    }
  }
  
  // interpret output from mdb command, pass to parent
  void parse(String buffer){
  
  // PC?
    if (processPC(buffer,parent)) return;
    
    // pin value?
    if (processPinValue(buffer,parent)) return;
  }
  
  // look for e.g.     address: 0x20
  boolean processPC(String buffer,PICSimMain parent)
  {
    int loc=buffer.indexOf("address:");
    if (loc < 0) return(false); // not a PC indicator

    String pc=buffer.substring(1+buffer.indexOf(":"));
    parent.setPC(pc);
    return true;
  }
  
  // look for RB0  Dout   LOW
  boolean processPinValue(String buffer, PICSimMain parent)
  {
    String[] parts=buffer.split("\t");

    if (parts.length < 3) return(false);
    if (parts[0].length() != 3) return(false);
    if (parts[0].charAt(0)!='R') return(false);
    char ab=parts[0].charAt(1);
    if ((ab != 'A') && (ab != 'B')) return(false);

    int pin;
    try{
      pin=Integer.parseInt((parts[0].charAt(2)) + "");
    } catch(Exception e){
      return (false); // not a valid pin numbers
    }
    
    // update pins data and enable/disable RAB input checkboxes
    parent.raEnable(ab,pin,parts[1].equals("Din"));
    
    // analog in?
    parent.anEnable(ab,pin,parts[1].equals("Ain"));
    
    // if this is not a DOut, nullify the LED display
    parent.ledEnable(ab,pin,parts[1].equals("Dout"));
    
    //is this an output pin?
    if (!parts[1].equals("Dout")) return(false); // not a digital output
    
    // yes - update the LED display
    boolean value=(parts[2].equals("HIGH")); 
    parent.showRAB(ab,pin,value);
    return(true);
  }
}