import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.swing.JOptionPane;
import javax.swing.JSlider;

/**
 * main thread for starting MDB process, sending commands and
 * stepping the simulation.
 * 
 * @author nick
 *
 */
public class MainThread extends Thread
{
  PICSimMain parent; // main frame
  InputThread it;  // read from stdout of mdb command
  int delay=0; // inter-step delay
  
  // exit request
  boolean doExit=false; // set to force exit
  
  // file loading
  String fname="";
  boolean doFileLoad=false; // set to load file
  
  // digital pin stimulus
  boolean doPinWrite=false;
  String pinName="";
  String pinValue="";
  
  // analog input
  boolean doAnWrite=false; // uses above pinName and pinValue
  
  // reset
  boolean doReset=false;
  
  MainThread(PICSimMain parent)
  {
    this.parent=parent;
  }
  
  public void run()
  {
    Runtime rt = Runtime.getRuntime();
    //String[] commands = {"/usr/bin/mdb"}; // what about Windows???
    //String[] commands = {"mdb"}; // what about Windows???
    //String[] commands={"c:\\Program Files (x86)\\Microchip\\MPLABX\\v5.10\\mplab_platform\\bin\\mdb.bat"};
    Process proc=null;
    boolean doMore=true; // in case we fail first time
    
    while (doMore){
      try {
        proc = rt.exec(parent.mdbCommand);
        doMore=false;
      } catch (IOException e) { // can't open command in .picsimrc
        JOptionPane.showMessageDialog(parent, "Can't execute command: " + e + "\n Try removing " + System.getProperty("user.home") + File.separator + ".picsimrc and re-running");
        System.exit(0);
      }
    }
    
    // okay make a printwriter for sending commands
    PrintWriter pw=new PrintWriter(proc.getOutputStream());
    
    // and start a thread to read stdout. That thread will update its record
    // of pin values etc.
    it=new InputThread(proc,parent);
    it.start();
   
   // now run the main sim loop
   mainSimLoop(pw);
  }
  
  void mainSimLoop(PrintWriter pw)
  {
    pw.println("device pic18f1220\nhwtool sim");
    pw.flush();
    while (!doExit){
      if (doFileLoad){ // load new file into simulator mem
        pw.println("program \"" + fname + "\"\nreset");
        doFileLoad=false;
      }
      
      if (doReset){
        pw.println("reset MCLR");pw.flush();
        doReset=false;
      }
      
      if (doPinWrite){
        pw.println("write pin " + pinName + " " + pinValue);pw.flush();
        doPinWrite=false;
      }
      
      if (doAnWrite){
        pw.println("write pin " + pinName + " " + pinValue);pw.flush();
        //System.out.println("write pin " + pinName + " " + pinValue);
        doAnWrite=false;
      }

      // read pin values
      readPinValues(pw);
      
      pw.println("stepi");pw.flush();
      sleep(delay);
    }
    //System.out.println("mt exiting");
    pw.println("quit");pw.close();System.exit(0);return;
  }
  
  String[] pinsToRead={"RA0","RA1","RA2","RA3","RA4","RA5","RA6","RA7",
                       "RB0","RB1","RB2","RB3","RB4","RB5","RB6","RB7"};

  // read all pin values
  void readPinValues(PrintWriter pw)
  {
    for (int i=0;i<pinsToRead.length;i++){
      pw.println("print pin " + pinsToRead[i]);pw.flush();
    }
  }
  
  // write a (digital) pin value
  void writeDigital(String name, String value)
  {
    doPinWrite=true;
    pinName=name;
    pinValue=value;
  }
  
  // set an analog pin voltage
  void writeAnalog(JSlider slider,int voltage)
  {
    int i;
    
    // figure out which slider this is
    for (i=0;i<parent.anSlider.length;i++){
      if (slider == parent.anSlider[i]) break;
    }

    if (i >= parent.anSlider.length){ // ooops!
      System.out.println("Error: slider not found!");
      return;
    }
    // i is index of slider
    doAnWrite=true;
    pinName="AN"+i;
    pinValue=String.format("%5.4gV",voltage/1000.);
    parent.anLabel[i].setText("AN"+i+": " + pinValue);
  }
  
  // setup file load
  void loadFile(String fname)
  {
    this.fname=fname;
    doFileLoad=true;
  }

  void doExit()
  {
    doExit=true;
  }
  
  void sleep(int ms)
  {
    try {
		Thread.sleep(ms);
	} catch (InterruptedException e) {
	}
  }
  
  void setDelay(int delay)
  {
    this.delay=delay;
  }
  
  void doReset()
  {
    doReset=true;
  }
}