import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ANListener implements ChangeListener{
  PICSimMain parent; // main frame
  
  ANListener(PICSimMain parent)
  {
    this.parent=parent; // save so we can tell main frame to update values and enable/disable sliders
  }

  public void stateChanged(ChangeEvent arg0) {
    JSlider slider=(JSlider)arg0.getSource();
    parent.mainThread.writeAnalog(slider,slider.getValue()); // this will also update the label
  }
}
