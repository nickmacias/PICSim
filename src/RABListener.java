import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;

public class RABListener implements ActionListener{
  PICSimMain parent; // main frame
    
  RABListener(PICSimMain parent)
  {
    this.parent=parent;
  }
    
  public void actionPerformed(ActionEvent arg0) {
    JCheckBox box=(JCheckBox)arg0.getSource();
    parent.mainThread.writeDigital(box.getText(),box.isSelected()?"HIGH":"LOW");
  }
}
